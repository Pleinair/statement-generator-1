package com.example.statementgenerator;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.List;

public class GeneratorFragment extends Fragment {
    public List<String> profiles;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.generator_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Spinner profileList = (Spinner) getView().findViewById(R.id.profiles);
        profiles = ((MainActivity)getActivity()).getProfiles();// Should be replaced with proper profile list (from text file?)
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, profiles);
        profileList.setAdapter(adapter); // populate spinner with profile list

        profileList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                MainActivity main = (MainActivity)getActivity();
                main.setProfile(profiles.get(position));
                Toast.makeText(getActivity(), "Changed to profile: '"+ main.getProfile() + "'", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(getActivity(), "Please pick a valid profile", Toast.LENGTH_LONG).show();
            }
        });

        profileList.setSelection(0);

        Button generateButton = (Button) getView().findViewById(R.id.generate_button);
        generateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: implement MJ's generator
                Toast toast = Toast.makeText(getActivity(), "Testing", Toast.LENGTH_SHORT);
                toast.show();

                TextView result = (TextView) getView().findViewById(R.id.generated_result);

                result.setText("Testing"); // should be set with MJ's generated text
            }
        });
    }
}
