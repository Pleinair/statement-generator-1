package com.example.statementgenerator;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ProfileFragment extends Fragment {
    List<String> profiles;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.profile_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LinearLayout linearLayout = getView().findViewById(R.id.profile_linear_layout);

        profiles = ((MainActivity)getActivity()).getProfiles();
        for(String profile: profiles){
            // TODO: On click events
            // TODO: Highlight on hover for profiles

            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    1.0f
            );

            LinearLayout.LayoutParams param2 = new LinearLayout.LayoutParams(
                    900,
                    200,
                    2.0f
            );

            LinearLayout.LayoutParams param3 = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    200,
                    1.0f
            );

            LinearLayout newLayout = new LinearLayout(getActivity());
            newLayout.setLayoutParams(param);

            TextView newText = new TextView(getActivity());
            newText.setLayoutParams(param2);

            Button newButton = new Button(getActivity());
            newButton.setLayoutParams(param3);

            newText.setText(profile);
            newText.setPadding(20,20,20,20);

            newButton.setText("Delete");
            newButton.setPadding(20,20,20,20);

            newLayout.addView(newText);
            newLayout.addView(newButton);

            linearLayout.addView(newLayout);
        }
    }
}
